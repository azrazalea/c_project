My Echo
=========
Problem
---------
Take a variable number of unsigned integers [in a binary stream] from STDIN and output them to STDOUT.

Solution
---------
Uses fread to grab an array of numbers from the stream until the stream ends. During this, it uses a for loop to print every number from said array.

My Sort
========
Problem
---------
Take a variable number of unsigned integers [in a binary stream] from STDIN and sort them using the quicksort algorithm, then output them to STDOUT.

Solution
--------
Firstly, we pull arrays of numbers using fread until we have all of them, putting them in our grand numbers array while we go. After this, we call an in-place recursive version of quicksort to sort the numbers, then output them to STDOUT.
